﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RotatingPlane
{
    public partial class Form2 : Form
    {
        Bitmap bmp = new Bitmap(400, 400);
        Graphics g;
        // Settings
        int triangleHeight = 50;
        int triangleBase = 100;
        int circleRadius = 50;
        int rectangleLength = 100;
        int rectangleWidth = 20;
        int distanceBetween = 20;

        int radius = 400 / 2 - 50;
        int center = 350 / 2;
        int angle = 0;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load_1(object sender, EventArgs e)
        {
            g = Graphics.FromImage(bmp); //Binding the graphics to the Bitmap
            pictureBox1.Image = bmp;
            RotatingTimer.Start();
        }

        private Point LineCoordTriangle1(int angleIn, int radius, int center)
        {
            var point = new Point();
            angleIn %= 360;
            angleIn *= 1;

            if (angleIn >= 0 && angleIn <= 180)
            {
                point.X = center + (int)((circleRadius + triangleHeight) * Math.Sin(Math.PI * angleIn / 180));
                point.Y = center - (int)((circleRadius + triangleHeight) * Math.Cos(Math.PI * angleIn / 180));
            }
            else
            {
                point.X = center - (int)((circleRadius + triangleHeight) * -Math.Sin(Math.PI * angleIn / 180));
                point.Y = center - (int)((circleRadius + triangleHeight) * Math.Cos(Math.PI * angleIn / 180));
            }
            return point;
        }

        private Point LineCoordTriangle2(int angleIn, int radius, int center)
        {
            var point = new Point();
            angleIn %= 360;
            angleIn *= 1;

            if (angleIn >= 0 && angleIn <= 180)
            {
                point.X = center + (int)(circleRadius * Math.Sin(Math.PI * angleIn / 180));
                point.Y = center - (int)(triangleBase / 2 * Math.Cos(Math.PI * angleIn / 180));
            }
            else
            {
                point.X = center - (int)(circleRadius * -Math.Sin(Math.PI * angleIn / 180));
                point.Y = center - (int)(triangleBase / 2 * Math.Cos(Math.PI * angleIn / 180));
            }
            return point;
        }

        /*private Point[] LineCoordLeftRectangle(int angleIn, int radius, int center)
        {
            angleIn %= 360;
            angleIn *= 1;

            var point1 = new Point();
            var point2 = new Point();
            var point3 = new Point();
            var point4 = new Point();
            if (angleIn >= 0 && angleIn <= 180)
            {
                point1.X = center + (int)((circleRadius + rectangleLength - 5) * Math.Sin(Math.PI * angleIn / 180));
                point1.Y = center - (int)((distanceBetween / 2 + rectangleWidth) * Math.Cos(Math.PI * angleIn / 180));
                point2.X = center + (int)((circleRadius - 5) * Math.Sin(Math.PI * angleIn / 180));
                point2.Y = center - (int)((distanceBetween / 2 + rectangleWidth) * Math.Cos(Math.PI * angleIn / 180));
                point3.X = center + (int)((circleRadius - 5) * Math.Sin(Math.PI * angleIn / 180));
                point3.Y = center - (int)((distanceBetween / 2) * Math.Cos(Math.PI * angleIn / 180));
                point4.X = center + (int)((circleRadius + rectangleLength - 5) * Math.Sin(Math.PI * angleIn / 180));
                point4.Y = center - (int)((distanceBetween / 2) * Math.Cos(Math.PI * angleIn / 180));
            }
            else
            {
                point1.X = center - (int)((circleRadius + rectangleLength - 5) * -Math.Sin(Math.PI * angleIn / 180));
                point1.Y = center - (int)((distanceBetween / 2 + rectangleWidth) * Math.Cos(Math.PI * angleIn / 180));
                point2.X = center - (int)((circleRadius - 5) * -Math.Sin(Math.PI * angleIn / 180));
                point2.Y = center - (int)((distanceBetween / 2 + rectangleWidth) * Math.Cos(Math.PI * angleIn / 180));
                point3.X = center - (int)((circleRadius - 5) * -Math.Sin(Math.PI * angleIn / 180));
                point3.Y = center - (int)((distanceBetween / 2) * Math.Cos(Math.PI * angleIn / 180));
                point4.X = center - (int)((circleRadius + rectangleLength - 5) * -Math.Sin(Math.PI * angleIn / 180));
                point4.Y = center - (int)((distanceBetween / 2) * Math.Cos(Math.PI * angleIn / 180));
            }

            Point[] points = {point1, point2, point3, point4};
            return points;
        }*/

        private void RotatingTimer_Tick(object sender, EventArgs e)
        {
            g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            pictureBox1.Image = bmp;

            var brush = new SolidBrush(Color.Blue);
            int triangleRadius = (int)Math.Sqrt(Math.Pow(circleRadius, 2) + Math.Pow(triangleBase / 2, 2));
            g.FillEllipse(brush, center - circleRadius, center - circleRadius, circleRadius * 2, circleRadius * 2);
            Point[] points = {LineCoordTriangle1(angle + 90, circleRadius + triangleHeight, center), LineCoordTriangle2(angle, triangleRadius, center), LineCoordTriangle2(angle + 180, triangleRadius, center) };
            g.FillPolygon(brush, points);

            /*int rectangleRadius = (int)Math.Sqrt(Math.Pow(circleRadius + rectangleLength, 2) + Math.Pow(distanceBetween / 2 + rectangleWidth, 2));
            g.FillPolygon(brush, LineCoordLeftRectangle(angle + 270, rectangleRadius, center));*/  

            g.Dispose();
            if (angle == 360)
            {
                angle = 0;
            }
            else
            {
                angle++;
            }
        }
    }
}
